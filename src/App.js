import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route,Link} from "react-router-dom";
import Menu  from './components/Menu';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faIgloo } from '@fortawesome/free-solid-svg-icons'

library.add(faIgloo)




class App extends Component {
  render() {
    return (
      <div className="App">
      <Router >
      <div>
         <Route  path="/restaurant/:id" component={Menu} />
       </div>
      </Router>
      </div>
    );
  }
}

export default App;
